.. -*- coding: utf-8 -*-

========================================================
Configurando y personalizando Plone 4 a través de la Web
========================================================

**Copyright**

**Autores originales**

*Plone y el logo Plone*

    .. figure:: _static/plone-logo-48-white-bg.png
       :alt: Logotipo de Plone CMS.

    Plone® y el logotipo de Plone son marcas registradas por la `Fundación Plone`_.

    Plone® and the Plone Logo are registered trademarks of `The Plone Foundation`_.

------

*The Mastering Plone 4 Training*

    Los capítulos 1, 2, 3 están licenciado bajo `Creative Commons Attribution 4.0 International License`_ 
    para los materiales del entrenamiento `The Mastering Plone 4 Training`_ de la 
    fundación Plone.

    |image0|
    |image1|

    * Copyright © 2014 - 2020 `The Plone Foundation`_.

    Los capítulos 4, 5 y Los apéndices A, B, C, D, E, F, G están licenciado bajo 
    `Creative Commons Atribución-CompartirIgual 3.0 Venezuela (CC BY-SA 3.0 VE)`_.

    |image2|
    |image3|

------

*Traducción al Español*

    Todas esta traducción de esta documentación es licencia bajo la Creative Commons
    Reconocimiento-CompartirIgual 3.0 Venezuela por:

    |image2|
    |image3|

    * Copyright © 2010 - 2021 `Covantec R.L.`_. Algunos derechos reservados.

      * 2010 - 2021 Leonardo J. Caballero G. <leonardoc@plone.org>.

      * 2014 Carlos J Morales G. <carlosm0177@gmail.com>.

------

.. seealso:: 
    Ver la sección :ref:`Licenciamientos <licencias>` para información completa 
    sobre los términos y licencia.

.. |image0| image:: _static/cc_by_40_88x31.png
.. |image1| image:: _static/international_unported.png
.. |image2| image:: _static/cc_by-sa_30_88x31.png
.. |image3| image:: _static/ve.png

.. _Fundación Plone: https://plone.org/foundation/
.. _The Plone Foundation: https://plone.org/foundation/
.. _Covantec R.L.: https://github.com/Covantec
.. _Creative Commons Attribution 4.0 International License: https://creativecommons.org/licenses/by/4.0/deed.es
.. _The Mastering Plone 4 Training: https://training.plone.org/4/
.. _Creative Commons Atribución-CompartirIgual 3.0 Venezuela (CC BY-SA 3.0 VE): https://creativecommons.org/licenses/by-sa/3.0/ve/

.. toctree::
    :numbered: 1

    configuring_customizing
    extending
    addons
    custom-favicon
    zmi/index
