.. -*- coding: utf-8 -*-

.. _glosario:

========
Glosario
========

.. sidebar:: Sobre este artículo

    :Autor(es): Leonardo J. Caballero G.
    :Correo(s): leonardoc@plone.org
    :Compatible con: Plone 3.x, Plone 4.x
    :Fecha: 11 de Enero de 2021

Este es un glosario para algunas definiciones usadas en esta documentación, todavía en gran parte esta *en construcción*.

.. glossary:: :sorted:

    .po
        El formato de archivo utilizado por el sistema de traducción :term:`gettext`. 
        http://www.gnu.org/software/hello/manual/gettext/PO-Files.html

    Acquisition
        Del Ingles *Acquisition*, la adquisición, En pocas palabras, cualquier objeto 
        Zope puede adquirir cualquier objeto o propiedad de cualquiera de sus padres. 
        Es decir, si tiene una carpeta llamada *A*, que contiene dos recursos (un 
        documento llamado *homepage* y otra carpeta llamada *B*), una URL que apunta 
        a `http://.../A/B/homepage` funcionaría aunque *B* sea vacío. Esto se debe a 
        que Zope comienza a buscar a *homepage* en *B*, no la encuentra, y vuelve a *A*, 
        donde se encuentra. La realidad, inevitablemente, es más compleja que esto. 
        Para toda la historia, vea el `capítulo de Adquisición en el Libro Zope <https://zope.readthedocs.io/en/latest/zopebook/Acquisition.html>`_.

    AGX
        AGX es la abreviatura de :term:`ArchGenXML`.

    Archetypes
        Archetypes es un framework diseñado para facilitar la creación de aplicaciones para 
        Plone y :term:`CMF`. Su objetivo principal es proporcionar un método común para 
        crear objetos de contenido, basado en definiciones de esquema. Los campos se pueden 
        agrupar para editar, por lo que es muy simple crear formularios tipo *asistente*. 
        Archetypes es capaz de hacer todo el trabajo pesado necesario para iniciar un tipo 
        de contenido, lo que permite al desarrollador centrarse en otras cosas, como reglas 
        de negocio, planificación, escalado y diseño. Proporciona características tales como 
        autogeneración de vistas de edición y presentación. El código de arquetipos se puede 
        generar desde UML usando :term:`ArchGenXML`.

    ArchGenXML
        ArchGenXML es un generador de código para aplicaciones CMF / Plone (un :term:`Producto`) 
        basado en el marco :term:`Archetypes`. Este analiza XMI-Format de modelos UML como 
        (``.xmi``, ``.zargo``, ``.zuml``), creado con aplicaciones como ArgoUML, Poseidon o 
        ObjectDomain. Un breve tutorial para ArchGenXML está presente en el sitio plone.org.

    ATCT
        ATContentTypes: los tipos de contenido Plone escritos con :term:`Archetypes` que 
        reemplaza los tipos de contenido :term:`CMF` predeterminados en Plone 2.1 en adelante.

    BBB
        Al agregar (o dejar) un fragmento de código para compatibilidad con versiones anteriores, 
        usamos un marcador de comentario BBB con una fecha.

    Buildout
        `Buildout`_ es un sistema de construcción basado en Python para crear, ensamblar y desplegar 
        aplicaciones desde múltiples partes, algunas de las cuales pueden no estar basadas en 
        Python. Te permite crear una configuración de construcción y reproducir el mismo software 
        más adelante. Ver `buildout.org <http://www.buildout.org/en/latest/>`_.
    
    bundle
        Ver :term:`Paquete bundle`.

    Catalog
        El catálogo es un índice interno del contenido dentro de Plone para que pueda ser buscado. 
        El objeto de catálogo es accesible a través de ZMI como el objeto `portal_catalog`_.

    CMF
        El Content Management Framework es un marco para crear aplicaciones orientadas al contenido 
        dentro de Zope. Como formó la base del contenido de Plone desde el comienzo.

    Cheese shop
        Ver :term:`Python Package Index`.

    Collective
        Es un repositorio de código de comunidad para Products y otros complementos 
        Plone, y es un lugar útil para encontrar el último código para cientos de complementos 
        para Plone. En este repositorio se alienta a los desarrolladores de nuevos productos de 
        Plone a compartir su código a través del colectivo o comunidad para que otros puedan 
        encontrarlo, usarlo y contribuir con correcciones y mejoras.
        
        Si usted quiere publicar un *nuevo* producto en el repositorio *Git de Collective* 
        de Plone necesita *obtener acceso de escritura* y `seguir las reglas en github/collective`_.

    control panel
        Del Ingles *Control panel*, el panel de control es el lugar donde se pueden establecer muchos 
        parámetros de un sitio Plone. Aquí se pueden habilitar complementos, se pueden crear usuarios 
        y grupos, se puede establecer el flujo de trabajo y los permisos, y se pueden encontrar ajustes 
        para el idioma, el almacenamiento en caché y muchos más. Si tiene permisos de "Administrador del 
        sitio", puede encontrarlo en "Sitio -> Configuración del sitio" en sus herramientas personales.

    CSS
        Del Ingles *Cascading Style Sheets*, es una forma de separar el contenido de la presentación. 
        Plone usa esto extensivamente, y es un estándar web `documentado en el sitio web del W3C <http://www.w3.org/Style/CSS/>`_. Si desea aprender CSS, recomendamos `los recursos de W3Schools CSS <https://www.w3schools.com/Css/default.asp>`_ y la `referencia de CSS de SitePoint <https://www.sitepoint.com/html-css/css/>`_.

    Dexterity
        Dexterity es una alternativa a :term:`Archetypes` , el venerable marco de contenido de Plone. 
        Al ser más reciente, Dexterity ha podido aprender de algunos de los errores que se hicieron 
        Archetypes y, lo que es más importante, aprovechar algunas de las tecnologías que no existían 
        cuando se concibió Archetypes por primera vez. Dexterity se construye desde cero para apoyar 
        la creación de tipos a través de la Web. Dexterity también permite que los tipos se desarrollen
        conjuntamente a través de la Web y en el sistema de archivos. Por ejemplo, un esquema puede 
        escribirse en Python y luego extenderse a través de la Web.
        
    Declaración ZCML
        El uso concreto de una :term:`Directiva ZCML` dentro de un archivo :term:`ZCML`.

    Directiva ZCML
        Una "etiqueta" :term:`ZCML` como ``<include />`` o ``<utility />``.

    Document
        Un documento es una página de contenido, generalmente una pieza de texto independiente. Los 
        documentos se pueden escribir en varios formatos diferentes, texto sin formato, HTML o 
        (re)Structured Text. La página de inicio predeterminada para un sitio de Plone es un ejemplo 
        de un documento.

    DTML
        Lenguaje de marcado de plantilla de documento. DTML es un lenguaje de plantillas del lado del 
        servidor utilizado para producir piezas dinámicas de contenido, pero ahora :term:`ZPT` reemplaza 
        para contenido HTML y XML. Todavía se usa con moderación para contenido no XML como SQL y correo/CSS.

    Dublin Core
        Dublin Core es un conjunto estándar de metadatos que permite la descripción de los recursos a los 
        fines del descubrimiento. Ver https://es.wikipedia.org/wiki/Dublin_Core

    Egg
        Ver :term:`Python Egg`.

    easy_install
        Una herramienta de línea de comandos para el descubrimiento automático y la instalación de paquetes 
        en un entorno de Python. El script ``easy_install`` es parte del paquete ``setuptools``, que utiliza 
        el :term:`Python Package Index` como fuente para los paquetes.

    esqueleto
        Los archivos y carpetas recreados por un usuario el cual los genero ejecutando 
        alguna plantilla ``templer`` (``PasteScript``).

    estructura
        1) Una clase Python la cual controla la generación de un árbol de carpetas 
        que contiene archivos.

        2) Una unidad de carpetas y archivos proveídos por el sistema ``templer`` para ser 
        usado en una plantilla o plantillas. Las estructuras proporcionan recursos 
        estáticos compartidos, que pueden ser utilizados por cualquier paquete en 
        el sistema de ``templer``.

        Las estructuras diferencian de las plantillas en que no proporcionan las :term:`vars`.

    Fecha de caducidad
        El último día debe aparecer un elemento en las búsquedas, listas de noticias, etc. Tenga en 
        cuenta que esto no elimina o desactiva el elemento, simplemente hace que no aparezca en las búsquedas.

        Esto es parte de los metadatos :term:`Dublin Core` que están presentes en todos los objetos Plone.

    Flujo de trabajo
        Es una forma muy poderosa de imitar los procesos de negocios; también es la forma en que se 
        manejan las configuraciones de seguridad en Plone.

    Flujo de trabajos
        Plural del termino :term:`Flujo de trabajo`.

    filesystem
        Del Ingles ``File system``, referido al sistema de archivo del sistema operativo.

    GenericSetup
        Un sistema de configuración basado en XML para aplicaciones Zope y Plone.

    gettext
        Herramienta de traducción de software estándar de UNIX. Ver http://www.gnu.org/software/gettext/
    
    Grok
        Ver la documentación del proyecto `Grok`_.

    Instalación de Zope
        El software propio del servidor de aplicaciones.
    
    Instancia de Zope
        Ver :term:`Zope instance`.

    i18n
        i18n es una abreviatura de "internacionalización" (la letra I, 18 letras, la letra N) y se refiere 
        al proceso de preparación de un programa para que pueda ser utilizado en múltiples idiomas sin 
        alterar aún más la fuente. Plone está completamente internacionalizado.

    i18ndude
        Herramienta de soporte para crear y actualizar catálogos de mensajes desde el código fuente instrumentado.

    JSON
        Notación de objetos de JavaScript. JSON es un estándar abierto basado en texto ligero diseñado para 
        el intercambio de datos legible por humanos. En resumen, es una cadena que se parece a una matriz de 
        JavaScript, pero está restringida a 6 tipos de datos simples. Puede ser analizado por muchos lenguajes 
        de programación.

    KSS
        Kinetic Style Sheets es un marco del lado del cliente para implementar interfaces de usuario ricas con 
        funcionalidad AJAX. Permite asociar acciones a elementos usando una sintaxis de reglas similar a CSS. 
        KSS se agregó a Plone en Plone 3 y se eliminó en Plone 4.3, porque JQuery lo dejó obsoleto.

    Kupu
        Kupu es el componente de editor gráfico HTML fácil de usar que solía incluirse con Plone, comenzando
        con la versión 2.1. Desde entonces ha sido reemplazado por :term:`TinyMCE`.

    l10n
        La localización es la preparación real de los datos para un idioma en particular. Por ejemplo, 
        Plone es consciente y tiene localización para varios idiomas. El término l10n está formado por 
        la primera y la última letra de la palabra y el número de letras intermedias.

    Layer
        Una ``Layer`` es un conjunto de plantillas y scripts que se presentan al usuario. Al combinar estas 
        capas, crea lo que se conoce como :term:`skin`. El orden de las capas es importante, las capas 
        superiores se examinarán primero al renderizar una página. Cada capa es una entrada en 
        ``portal_skins`` -> 'Contents', y es generalmente es una vista de directorio del sistema de archivos 
        o una carpeta.

    LDAP
        Protocolo ligero de acceso a directorios. Un protocolo de Internet que proporciona una especificación 
        para acceso de directorio de usuario por cable, sintaxis de atributo, representación de nombres 
        completos, filtros de búsqueda, un formato de URL, un esquema para información centrada en el usuario, 
        métodos de autenticación y seguridad de capa de transporte. Ejemplo: un cliente de correo electrónico 
        puede conectarse a un servidor LDAP para buscar una dirección de correo electrónico para una persona 
        por el nombre de una persona.

    local command
        Una clase `Paste`_ la cual provee funcionalidad adicional a una estructura de esqueleto de proyecto 
        que ha sido generada.

    Manager
        *Manager* es el rol estándar de seguridad en Zope. Un usuario con la función Administrador tiene TODOS 
        los permisos excepto el permiso Tomar propiedad. También conocido comúnmente como Administrador o raíz 
        en otros sistemas.

    METAL
        Lenguaje de atributos de plantillas de expansión de macros. Ver :term:`ZPT`.

    Monkey patch
        Un Monkey patch es una forma de modificar el comportamiento de Zope o un :term:`Producto` sin alterar 
        el código original. Útil para soluciones que tienen que vivir junto con el código original por un 
        tiempo, como revisiones de seguridad, cambios de comportamiento, etc.

        El término "Monkey patch" parece haberse originado de la siguiente manera: primero fue un "guerrilla 
        patch", que hace referencia al código que cambia sigilosamente otro código en tiempo de ejecución sin 
        reglas. En Zope 2, a veces estos parches entran en conflicto. Este término dio vueltas alrededor de 
        Zope Corporation por un tiempo. Sin embargo, las personas lo escucharon como "gorilla patch", ya que 
        las dos palabras se parecen mucho, y la palabra gorila se escucha con más frecuencia. Entonces, cuando 
        alguien creó un guerrilla patch con mucho cuidado y trató de evitar cualquier batalla, trataron de 
        hacerlo sonar con menos fuerza llamándolo monkey patch. El término se estancó.

    módulo
        Del Ingles ``module``, es un archivo fuente Python; un archivo en el sistema
        de archivo que típicamente finaliza con la extensión ``.py`` o ``.pyc``. Los módulos
        son parte de un :term:`paquete`.

    Namespace package
        Una característica de herramientas de configuración que hace posible distribuir paquetes múltiples y 
        separados que comparten un único espacio de nombres de nivel superior. Por ejemplo, los paquetes 
        ``plone.theme`` y ``plone.portlets`` ambos comparten el  espacio de nombres de nivel superior 
        ``plone``, pero se distribuyen como :term:`Egg` separados. Cuando está instalado, el código fuente de 
        cada :term:`Egg` tiene su propio directorio (o posiblemente un archivo comprimido de ese directorio). 
        Los paquetes de espacio de nombres eliminan la necesidad de distribuir un paquete gigante plone, 
        con un directorio de nivel superior ``plone`` que contiene todos los hijos posibles.
    
    Nombre de puntos Python
        Es la representación Python del "camino" para un determinado objeto / módulo / función,
        por ejemplo, ``Products.GenericSetup.tool.exportToolset``. A menudo se utiliza como 
        referencia en configuraciones ``Paste`` y ``setuptools`` a cosas en Python.

    OpenID
        Un sistema de identidad distribuida. Con un único proveedor de URI, una persona puede iniciar sesión 
        en cualquier sitio web que acepte OpenID utilizando el URI y una contraseña. Plone implementa OpenID 
        como un complemento :term:`PAS`.

    PAS
        El Pluggable Authentication Service (PAS) es un marco para manejar la autenticación en Zope 2. PAS es 
        un objeto de carpeta ``acl_users`` Zope que usa "complementos" que pueden implementar varias 
        interfaces de autenticación (por ejemplo, :term:`LDAP` y :term:`OpenID`) que se conectan al framework 
        PAS. Zope 3 también usa un diseño inspirado en PAS. PAS se integró a Plone en el Sprint de San Jose 
        en 2005.

    PLIP
        Propuesta de mejora de PLone (al igual que las PEP de Python: Propuestas de mejora de Python). Estos 
        son documentos escritos para estructurar y organizar propuestas para la mejora de Plone.

        Motivación, entregas, riesgos y una lista de personas dispuestas a hacer el trabajo debe ser incluido. 
        Este documento se envía al `Equipo del framework <https://plone.org/community/framework>`_, que revisa 
        la propuesta y decide si es adecuada para ser incluida en la próxima versión de Plone o no.

        Vea más información sobre cómo escribir un `PLIP <https://docs.plone.org/develop/coredev/docs/plips.html>`_.

    PYTHONPATH
        Ver :term:`Python path`.

    Paquete bundle
        Este paquete consististe en un archivo comprimido con todos los módulos que son 
        necesario compilar o instalar en el :term:`PYTHONPATH` de tu interprete ``Python``.

    Plonista
        Un Plonista es un miembro de la comunidad Plone. Puede ser alguien que ama a Plone, o usa Plone, o 
        alguien que difunde el conocimiento de Plone y Plone. También puede ser alguien que sea desarrollador 
        de Plone, o puede ser todo lo anterior.
    
    Paquete Python
        Un término general que describe un módulo de Python redistribuible. En el nivel más básico, un 
        paquete es un directorio con un archivo ``__init__.py``, que puede estar en blanco.

    Paquetes Python
        Plural del termino :term:`Paquete Python`.

    Producto
        Es una terminología usada por la comunidad Zope / Plone asociada a
        cualquier implementación de módulos / complementos y agregados que amplíen la
        funcionalidad por defecto que ofrece Zope / Plone. También son conocidos como
        *"Productos de terceros"* del Ingles `Third-Party Products`_.
        Es un módulo específico de Plone que extiende la funcionalidad de Plone y se puede administrar 
        a través del Panel de control de Plone. Los productos Plone a menudo integran módulos no 
        específicos de Plone para usar dentro del contexto de Plone.
    
    Producto Plone
        Es un tipo especial de paquete Zope usado para extender las funcionalidades
        de Plone. Se puede decir que son productos que su ámbito de uso es solo en el
        desde la interfaz gráfica de Plone.
    
    Producto Zope
        Es un tipo especial de paquete Python usado para extender Zope. En las
        antiguas versiones de Zope, todos los productos eran carpetas que se ubican
        dentro de una carpeta especial llamada ``Products`` de una instancia Zope;
        estos tendrían un nombre de módulo Python que empiezan por "**Products.**".
        Por ejemplo, el núcleo de Plone es un producto llamado ``CMFPlone``, conocido 
        en Python como `Products.CMFPlone`_.
        
        Este tipo de productos esta disponibles desde la `interfaz administrativa de Zope (ZMI)`_ 
        de `su instalación`_ donde deben acceder con las credenciales del usuario 
        Administrador de Zope. Muchas veces el producto simplemente no hay que 
        instalarlo por que se agregar automáticamente.

    Productos
        Plural del termino :term:`Producto`.

    Productos Plone
        Plural del termino :term:`Producto Plone`.

    Productos Zope
        Plural del termino :term:`Producto Zope`.

    PyPI
        Ver :term:`Python Package Index`.

    Python Egg
        Un formato de paquete de Python ampliamente utilizado que consiste en un archivo comprimido 
        ``.tar.gz`` o ``.zip`` con información de metadatos. Fue presentado por `setuptools`_.

        Una forma de empaquetar y distribuir paquetes de Python. Cada :term:`Egg` contiene un archivo 
        ``setup.py`` con metadatos (como el nombre del autor y la dirección de correo electrónico y la 
        información de licencia), así como información sobre las dependencias. ``setuptools``, la biblioteca 
        de Python que impulsa el mecanismo del :term:`Egg`, puede encontrar y descargar dependencias 
        automáticamente para los *Eggs* que instales. Incluso es posible que dos *Eggs* diferentes utilicen 
        simultáneamente diferentes versiones de la misma dependencia. Los *Eggs* también admiten una 
        característica llamada *entry points*, un tipo de mecanismo genérico de complemento.

    Python Package Index
        El índice de la comunidad Python de miles de paquetes de Python descargables. Está disponible como un 
        sitio web para navegar, con la capacidad de buscar un paquete en particular. Más importante aún, las 
        herramientas de empaquetado basadas en ``setuptools`` (más notablemente, ``buildout`` y 
        ``easy_install``) pueden consultar este índice para descargar e instalar *Eggs* automáticamente. 
        También conocido como Cheese Shop o Python Package Index - PyPI. Es el servidor central 
        de :term:`Python Egg` ubicado en la dirección https://pypi.org/project/.

    Python path
        El orden y la ubicación de las carpetas en las que el intérprete de Python buscará módulos. Está 
        disponible en Python vía ``sys.path``. Cuando Zope se ejecuta, esto normalmente incluye los módulos 
        globales de Python que componen la biblioteca estándar, el directorio de paquetes de sitio del 
        intérprete, donde se instalan los módulos y *Eggs* "globales" de terceros, el *Zope software home* 
        y el directorio ``lib/python`` dentro de la instancia home. Es posible que los scripts de Python 
        incluyan rutas adicionales en la ruta de Python durante el tiempo de ejecución. Esta habilidad es 
        utilizada por ``zc.buildout``.

        .. tip:: *En Linux*
            Es una lista de nombre de directorios, que contiene librerías Python, con la misma 
            sintaxis como la declarativa ``PATH`` del shell del sistema operativo.

    part
        En la herramienta :term:`buildout`, es un conjunto opciones que le permite a usted 
        construir una pieza de la aplicación.

    paquete
        Ver :term:`Paquete Python`.

    paquete Egg
        Ver :term:`Python Egg`.

        Es una forma de empaquetar y distribuir paquetes Python. Cada Egg contiene
        un archivo :file:`setup.py` con metadata (como el nombre del autor y la correo
        electrónico y información sobre el licenciamiento), como las dependencias del
        paquete.

        La herramienta del `setuptools`_, es la librería Python que permite
        usar el mecanismo de paquetes egg, esta es capaz de encontrar y descargar
        automáticamente las dependencias de los paquetes Egg que se instale. 

        Incluso es posible que dos paquetes Egg diferentes necesiten utilizar simultáneamente
        diferentes versiones de la misma dependencia. El formato de paquetes Eggs
        también soportan una función llamada ``entry points``, una especie de
        mecanismo genérico de plug-in. Mucha más detalle sobre este tema se encuentra
        disponible en el `sitio web de PEAK`_.

    paquetes Egg
        Plural del termino :term:`paquete Egg`.

    plantilla
        1) Una clase Python la cual controla la generación de un esqueleto. Las 
        plantillas contiene una lista de variables para obtener la respuesta de un 
        usuario. Las plantillas son ejecutadas con el comando ``templer`` suministrando 
        el nombre de la plantilla como un argumento ``templer basic_namespace my.package``.

        2) Los archivos y carpetas proveídas un paquete ``templer`` como contenido a ser 
        generado. Las respuestas proporcionadas por un usuario en respuesta a las variables 
        se utilizan para rellenar los marcadores de posición en este contenido.
    
    profile
        Una configuración "predeterminada" de un sitio, que se define en el sistema de
        archivos o en un archivo tar.

    RAD
        Desarrollo rápido de aplicaciones: un término aplicado a las herramientas de desarrollo para referirse 
        a cualquier cantidad de características que facilitan la programación. :term:`Archetypes` y 
        :term:`ArchGenXML` son ejemplos de estos del universo Plone.

    Request
        Cada vista de página de un cliente genera una request a Plone. Esta request entrante se encapsula en 
        un objeto Zope *request*, generalmente llamado REQUEST (o "request" en minúsculas en el caso de ZPT).

    ResourceRegistries
        Una parte de la infraestructura de Plone que permite que las declaraciones de CSS / Javascript estén 
        contenidas en archivos lógicos separados antes de ser finalmente anexadas a los archivos existentes de 
        CSS / Javascript de Plone en la entrega de la página. Principalmente permite a los autores de 
        productos "registrar" nuevas CSS / Javascript sin necesidad de tocar las plantillas de Plone, pero 
        también permite la inclusión selectiva de archivos CSS / Javascript y reduce la carga de la página al 
        minimizar las llamadas individuales a bloques separados de archivos CSS / Javascript. Encontrado en 
        :term:`ZMI` debajo ``portal_css`` y ``portal_javascript``.

    recipe
        En la herramienta :term:`buildout`, es el software usado para crear partes de 
        una instalación basada en sus opciones. Mas información consulte el articulo 
        `Recipes Buildout`_.

    reStructuredText
        El lenguaje estándar de marcado de texto plano utilizado para la documentación de Python: https://docutils.sourceforge.io/rst.html

        `reStructuredText <https://docutils.sourceforge.io/rst.html>`_ es una sintaxis de marcado de texto 
        simple fácil de leer y un sistema analizador. Es útil para la documentación del programa en línea 
        (como las Python docstrings), para crear rápidamente páginas web simples y para documentos 
        independientes. reStructuredText está diseñado para ser extensible para dominios de aplicación 
        específicos. El analizador reStructuredText es un componente de `Docutils <https://docutils.sourceforge.io/index.html>`_.

        reStructuredText es una revisión y reinterpretación de los sistemas de marcado ligero 
        `StructuredText <https://plone.org/documentation/glossary/stx>`_ y 
        `Setext <https://docutils.sourceforge.io/mirror/setext.html>`_.

    Skin
        Se utiliza una colección de capas de plantilla (ver :term:`layer`) como ruta de búsqueda cuando se 
        representa una página y las diferentes partes buscan fragmentos de plantilla. Las Skins se definen 
        en el :term:`ZMI` en la herramienta``portal_skins``. Se usa para personalizaciones de presentaciones 
        y códigos.

    Software home
        El directorio dentro de la instalación Zope (en el sistema de archivos) que contiene todo el código 
        Python que forma el núcleo del servidor de aplicaciones Zope. Los diversos paquetes Zope se 
        distribuyen aquí. También se conoce como la variable de entorno ``SOFTWARE_HOME``. Varía de un 
        sistema a otro, dependiendo de dónde usted o su sistema de embalaje haya instalado Zope. Puede 
        encontrar el valor de esto en *ZMI > Control Panel*.

    Sprint
        Basado en ideas de la comunidad de programación extrema (XP). Un sprint es una sesión de desarrollo 
        enfocada de tres a cinco días, en la cual los desarrolladores se emparejan en una sala y se enfocan en 
        construir un subsistema en particular. Ver https://plone.org/events/sprints

    STX
        El Structured Text o texto estructurado es una técnica de marcado simple que es útil cuando no desea 
        recurrir a HTML para crear contenido web. Utiliza sangrado para estructura y otras marcas para 
        formatear. Ha sido reemplazado por :term:`reStructuredText`, pero algunas personas todavía prefieren 
        la versión anterior, ya que es más simple.

    Syndication
        La sindicación muestra los varios objetos actualizados más recientemente en una carpeta en formato 
        RSS. Este formato está diseñado para ser leído por otros programas.

    setup.py
        El archivo :file:`setup.py` es un :term:`módulo` de Python, que por lo general indica que
        el módulo / paquete que está a punto de instalar ha sido empacado y distribuidos
        con ``Distutils``, que es el estándar para la distribución de módulos de Python.
        
        Con esto le permite instalar fácilmente paquetes de Python, a menudo es suficiente
        para escribir: ::

            python setup.py install

        Entonces el módulo Python se instalará.

        .. seealso::
            - https://docs.python.org/2/install/index.html

    slug
        Un :term:`ZCML` *slug* es un archivo de una línea creado en el directorio ``etc/package-includes`` 
        de una instancia Zope, con un nombre como ``my.package-configure.zcml``. El contenido del archivo 
        sería algo así como: ``<include package="my.package" file="configure.zcml" />``.

        Esta es la forma de Zope 3 de cargar un paquete en particular.

        .. tip::
            Estas configuraciones ya no están más en uso, pueden ser eliminados agregando 
            las configuraciones del paquete `z3c.autoinclude`_.

    TAL
        Lenguaje de atributo de la plantilla. Ver :term:`ZPT`.

    TALES
        Sintaxis de expresión :term:`TAL`. La sintaxis de las expresiones utilizadas en los atributos TAL.

    TinyMCE
        Un editor gráfico de HTML incluido con Plone.

    TODO
        El marcador TODO en el código fuente registra nuevas características, notas de optimización no 
        críticas, cambios de diseño, etc.

    Traceback
        Un "rastreo" o Traceback de Python es un mensaje de error detallado que se genera cuando se produce 
        un error al ejecutar el código de Python. Dado que Plone, corriendo encima de Zope, es una aplicación 
        de Python, la mayoría de los errores de Plone generarán un rastreo de Python. Si está presentando un 
        informe de problema relacionado con un error de Plone o Producto Plone, intente incluir una entrada 
        de registro de rastreo con el informe.

        Para encontrar el rastreo, verifique su archivo de registro ``event.log``. Alternativamente, use la 
        ZMI para verificar el objeto ``error_log`` en su carpeta Plone. Tenga en cuenta que su Zope debe estar 
        ejecutándose en modo de *debug* o depuración para poder registrar las trazas.

        Se incluirá un rastreo en casi todas las entradas de error. Un traceback se verá más o menos así: 
        "Traceback (el último más interno): ... AttributeError: adaptaters" Pueden ser muy largos. La 
        información más útil generalmente se encuentra al final.

    traversal
        Publicar un objeto desde el :term:`ZODB` atravesando sus objetos principales, resolviendo la 
        seguridad y los nombres en el alcance. Vea el 
        `capítulo de Adquisición en el Libro Zope <https://zope.readthedocs.io/en/latest/zopebook/Acquisition.html>`_.

    TTP
        Del Ingles "Through the Plone - TTP", son las acciones que se llevan a cabo mediante la interfaz 
        "A través de Plone". Normalmente, es una forma perezosa de decirle que no debe agregar elementos 
        de la ZMI, como es el caso de agregar contenido, por ejemplo.

    TTW
        Este es un término general que significa que una acción puede realizarse "a través de la Web", en 
        lugar de, digamos, hacerse de manera programática.
    
    Temas / Apariencias
        Por lo general si un producto de Tema esta bien diseñado y implementado
        debe aplicarse de una ves al momento de instalarlo. En caso que no se aplique
        de una puede acceder a la sección `Configuración de Temas`_ y cambiar el
        **Tema predeterminado** por el de su gusto.
    
    Tipos de contenidos
        Los tipos de contenidos son productos que extienden la funcionalidad de
        **Agregar elemento** que permite agregar nuevos tipos de registros
        (Contenidos) a tu sitio. Esto quiere decir que si instala un tipo de
        contenido exitosamente debería poder acceder a usarlo desde el menú de
        **Agregar elemento** en el sitio Plone. Opcionalmente algunos productos
        instalan un panel de control del producto que puede acceder a este en la
        sección `Configuración de Productos Adicionales`_.

    UML
        El *lenguaje de modelado unificado*, es un lenguaje de modelado de propósito general que incluye 
        una notación gráfica estandarizada que se utiliza para crear un modelo abstracto de un sistema, 
        denominado *modelo UML*. Con el uso de :term:`ArchGenXML`, esto se puede utilizar para generar 
        código para aplicaciones CMF / Plone (un :term:`Producto`) basado en el framework Archetypes.

    VirtualHostMonster
        Una tecnología Zope que admite alojamiento virtual. Ver el 
        `mecanismo de reescritura de URL VirtualHostMonster <https://zope.readthedocs.io/en/latest/zopebook/VirtualHosting.html>`_.
    
    var
        Diminutivo en singular del termino :term:`variable`.

    vars
        Diminutivo en plural del termino :term:`variable`.

    variable
        1) Una pregunta que debe ser respondida por el usuario cuando esta generando una 
        estructura de esqueleto de proyecto usando el sistema de plantilla ``templer``. En este 
        caso una variable (var) es una descripción de la información requerida, texto de 
        ayuda y reglas de validación para garantizar la entrada de usuario correcta.
             
        2) Una declarativa cuyo valor puede ser variable o constante dentro de un programa 
        Python o en el sistema operativo.

    variables
        Plural del termino :term:`variable`.

    virtualenv
        ``virtualenv``, es una herramienta para crear un directorio de proyecto con un intérprete de Python 
        que está aislado del resto del sistema. Los módulos que instale en dicho entorno siguen siendo 
        locales y no afectan a su sistema Python u otros proyectos.

    Workflow
        Ver :term:`Flujo de trabajo`.

    XXX
        ``XXX``, es un marcador en los comentarios del código fuente que solo se debe usar durante el 
        desarrollo para tomar nota de las cosas que deben tenerse en cuenta antes de comprometerse con 
        una final (a la rama ``trunk`` o ``master``). Idealmente, uno no debería esperar ver XXX en el 
        software lanzado. El marcador ``XXX`` no se usará para registrar nuevas funciones, optimización 
        no crítica, cambios de diseño, etc. Si desea grabar cosas como esa, use los comentarios marcador 
        ``TODO``. Las personas que hacen un lanzamiento no deberían preocuparse por los ``TODO``, pero 
        deberían molestarse por encontrar las marcas ``XXX``.

    ZCA
        Zope Component Architecture (ZCA) es un framework de Python para soportar el diseño y la programación 
        basada en componentes. Es muy adecuado para desarrollar grandes sistemas de software Python. El ZCA 
        no es específico del servidor de aplicaciones web Zope: se puede usar para desarrollar cualquier 
        aplicación Python. De una guía completa de Zope Component Architecture .

    ZCML
        Siglas del termino en Ingles :term:`Zope Configuration Mark-up Language`.

    ZCML-slug
        Ver :term:`slug`.

    ZCatalog
        Ver :term:`Catalog`.

    Zope Component Architecture
        La `arquitectura de componentes de Zope`_ (ZCA), es un marco de trabajo en 
        Python que soporta el diseño y la programación basada en componentes. La ZCA funciona 
        muy bien al desarrollar sistemas de software grandes en Python. La ZCA no es específica 
        al servidor de aplicaciones Zope, se puede utilizar para desarrollar cualquier aplicación 
        Python. Quizás debería llamarse la *Arquitectura de Componentes de Python*.

    Zope Configuration Mark-up Language
        Lenguaje de marcado de configuración Zope. Es un dialecto XML utilizado por Zope 
        para las tareas de configuración. ``ZCML`` es capaz de realizar diferentes tipos 
        de declaración de configuración. Es utilizado para extender y conectar a los 
        sistemas basados en la :term:`Zope Component Architecture`.

        ``Zope 3`` separa la política del código real y moverlo a archivos de configuración 
        separados, generalmente un archivo ``configure.zcml`` en un buildout. Este archivo 
        configura la instancia de Zope. El concepto 'configuración' puede ser un poco engañoso 
        aquí y debería pensarse o tomarse más como cableado. 

        ``ZCML``, el lenguaje de configuración basado en ``XML`` que se utiliza para esto, 
        está diseñado para hacer declaraciones de seguridad y registro de componentes, en su 
        mayor parte.Al habilitar o deshabilitar ciertos componentes en ``ZCML``, puede configurar 
        ciertas políticas de la aplicación general. En Zope 2, habilitar y deshabilitar componentes 
        significa colocar o eliminar un determinado producto ``Zope 2``. Cuando está allí, se 
        importa y carga automáticamente. Este no es el caso en ``Zope 3``. Si no lo habilita 
        explícitamente, no se encontrará.

        El proyecto :term:`Grok` ha adoptado un enfoque diferente para el mismo problema, y permite 
        el registro de componentes, etc haciendo declarativa de código Python. Ambos enfoques son 
        posibles en Plone.

    ZEO
        Ver :term:`ZEO server`.

    ZEO server
        ZEO (Zope Enterprise Objects) es una solución de escala utilizada con Zope. El servidor ZEO es un 
        servidor de almacenamiento que permite múltiples instancias Zope, llamadas clientes ZEO, para 
        conectarse a una única base de datos. Los clientes ZEO pueden distribuirse en varias máquinas. 
        Para obtener información adicional, 
        `consulte el capítulo relacionado al libro de Zope <https://zope.readthedocs.io/en/latest/zopebook/ZEO.html>`_.

    ZMI
        La *Zope Management Interface*. Zope tiene una interfaz de administración integrada a la que se 
        puede acceder a través de la web. El acceso es tan simple como agregar ``/manage`` a su URL, por 
        ejemplo: ``http://localhost/manage`` - o visitar la Configuración de Plone y hacer clic en el enlace 
        de la *Interfaz de Administración de Zope* (Haga clic en 'View' para volver al sitio de Plone). 
        Tenga cuidado allí, sin embargo, es la "vista geek" de las cosas, y no es directo, ni lo protege 
        de hacer cosas estúpidas. :)

    ZODB
        La base de datos de objetos Zope es donde normalmente se almacena su contenido cuando usa Plone. 
        El backend de almacenamiento predeterminado de ZODB en versiones posteriores a Plone 3, es el 
        *filestorage*, que almacena la base de datos en el sistema de archivos en los archivos como 
        ``Data.fs``, normalmente, ubicados en el directorio ``var``.

    Zope instance
        El Zope instance o Instancia Zope es un proceso de sistema operativo que maneja la interacción HTTP 
        con una base de datos Zope (:term:`ZODB`). En otras palabras, el proceso del servidor web Zope. 
        Alternativamente, el código de Python y otros archivos de configuración necesarios para ejecutar 
        este proceso.

        Una instalación de Zope puede admitir instancias múltiples. Use la receta buildout 
        ``plone.recipe.zope2instance`` para crear nuevas instancias de Zope en un entorno buildout.

        Varias instancias de Zope pueden servir datos de un solo :term:`ZODB` usando un servidor ZEO en el backend.

    Zope product
        Un tipo especial de paquete de Python utilizado para extender Zope. En las versiones anteriores de 
        Zope, todos los productos eran directorios dentro del directorio especial de *Productos* de una 
        instancia Zope; estos tendrían un nombre de módulo de Python que comienza con ``Products``. Por 
        ejemplo, el núcleo de Plone es un producto llamado *CMFPlone*, conocido en Python como 
        ``Products.CMFPlone``.

    ZPL
        Licencia pública de Zope, una licencia de estilo BSD bajo licencia de Zope.

    ZPT
        *Zope Page Templates* es el lenguaje de plantillas que se utiliza para representar las páginas de 
        Plone. Se implementa como dos espacios de nombres XML, lo que permite crear plantillas luzcan 
        normalmente a HTML / XML para los editores de código fuente. 

        Ver la `referencia de las Zope Page Templates <https://zope.readthedocs.io/en/latest/zopebook/AppendixC.html>`_.

.. _Third-Party Products: https://old.plone.org/documentation/kb/add-ons/tutorial-all-pages
.. _Products.CMFPlone: https://pypi.org/project/Products.CMFPlone
.. _sitio web de PEAK: http://peak.telecommunity.com/DevCenter/setuptools
.. _seguir las reglas en github/collective: https://collective.github.io/
.. _Configuración de Temas: http://localhost:8080/Plone/@@skins-controlpanel
.. _Configuración de Productos Adicionales: http://localhost:8080/Plone/prefs_install_products_form
.. _Grok: http://grok-community-docs.readthedocs.io/en/latest/
.. _su instalación: http://localhost:8080/manage
.. _z3c.autoinclude: https://pypi.org/project/z3c.autoinclude
.. _Paste: http://paste.readthedocs.io/en/latest/index.html
.. _arquitectura de componentes de Zope: https://plone-spanish-docs.readthedocs.io/es/latest/programacion/zca/zca-es.html#zca-es
.. _Buildout: https://plone-spanish-docs.readthedocs.io/es/latest/buildout/replicacion_proyectos_python.html#que-es-zc-buildout
.. _ZMI: https://plone-spanish-docs.readthedocs.io/es/latest/zope/zmi/index.html
.. _portal_catalog: https://plone-spanish-docs.readthedocs.io/es/latest/zope/zmi/index.html#portal-catalog
.. _Recipes Buildout: https://plone-spanish-docs.readthedocs.io/es/latest/buildout/recipes.html
.. _setuptools: https://plone-spanish-docs.readthedocs.io/es/latest/python/setuptools.html
.. _interfaz administrativa de Zope (ZMI): https://plone-spanish-docs.readthedocs.io/es/latest/zope/zmi/index.html