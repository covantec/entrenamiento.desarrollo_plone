.. -*- coding: utf-8 -*-

========================================================
Manual del curso "Desarrollo y personalización de Plone"
========================================================

Repositorio de manuales y recursos del curso "Desarrollo y personalización de
Plone" realizado por Covantec R.L.

.. contents :: :local:

Estructura general
==================

La estructura general de contenidos esta confirmada por los principales archivos:

**00-capacitacion-desarrollo-plone.odt**
  Describe el contenido de la capacitación.

**01-0-zope-component-architecture.odp**
  Describe los contenidos del módulo *1* de la capacitación.

**01-1-guia-comprensiva-zca**
  Describe los contenidos del módulo *1* de la capacitación.

**02-zope-plone-a-traves-web**
  Describe los contenidos del módulo *2* de la capacitación.

**03-0-practicas-programacion-plone.odp**
  Describe los contenidos del módulo *3* de la capacitación.

**03-1-virtualenv.pdf**
  Describe los contenidos del módulo *3* de la capacitación.

**03-2-sistema-integracion-buildout.pdf**
  Describe los contenidos del módulo *3* de la capacitación.

**04-0-temas-en-plone.odp**
  Describe los contenidos del módulo *4* de la capacitación.

**04-1-browser-view-en-plone.odp**
  Describe los contenidos del módulo *4* de la capacitación.

**04-2-personalizar-visual-plone.odp**
  Describe los contenidos del módulo *4* de la capacitación.

**04-3-editor-temas-plone.pdf**
  Describe los contenidos del módulo *4* de la capacitación.

**05-0-viewlets-en-plone.odp**
  Describe los contenidos del módulo *5* de la capacitación.

**05-1-portlets-en-plone.odp**
  Describe los contenidos del módulo *5* de la capacitación.

**06-0-tipos-contenidos-plone.odp**
  Describe los contenidos del módulo *6* de la capacitación.

**06-1-mda-en-plone.odp**
  Describe los contenidos del módulo *6* de la capacitación.

**06-2-tipos-contenidos-dexterity.odp**
  Describe los contenidos del módulo *6* de la capacitación.

**07-perfiles-instalacion-plone.odp**
  Describe los contenidos del módulo *7* de la capacitación.

**08-0-seguridad-en-plone.pdf**
  Describe los contenidos del módulo *8* de la capacitación.

**08-1-usuario-grupos-roles.pdf**
  Describe los contenidos del módulo *8* de la capacitación.

**08-2-flujo-trabajo-politicas.pdf**
  Describe los contenidos del módulo *8* de la capacitación.

**09-formulario-bd-relacional.odp**
  Describe los contenidos del módulo *9* de la capacitación.

**10-0-pruebas-calidad-software.odp**
  Describe los contenidos del módulo *10* de la capacitación.

**10-1-manual-multimechanize**
  Describe los contenidos del módulo *10* de la capacitación.


Obtener la documentación
=========================

El almacenamiento de este material está disponible en un repositorio Git 
en Bitbucket.org "`entrenamiento.desarrollo_plone`_". Si usted tiene una
credenciales en este servidor y desea convertirse en un colaborador ejecute 
el siguiente comando: ::

  $ git clone https://bitbucket.org/covantec/entrenamiento.desarrollo_plone.git

Crear entorno virtual de Python para reconstruir este proyecto: ::

  $ sudo apt-get install python-setuptools python-pip 
  $ sudo apt-get install git latexmk texlive-latex-recommended texlive-latex-extra texlive-lang-spanish texlive-xetex
  $ sudo pip install virtualenv
  $ cd $HOME ; mkdir $HOME/virtualenv ; cd $HOME/virtualenv
  $ virtualenv sphinx
  $ source $HOME/virtualenv/sphinx/bin/activate

Instale Sphinx: ::

  (sphinx)$ pip install Sphinx

Módulo 1
--------

Ahora puede generar la documentación en PDF del módulo *1*; ejecute los siguientes comandos: ::

  (sphinx)$ cd entrenamiento.desarrollo_plone/01-1-guia-comprensiva-zca
  (sphinx)$ make xelatexpdf

Una ves generado el PDF se puede abrir desde el directorio ``build/latex/01-1-guia-comprensiva-zca.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, ...).

Módulo 2
--------

Ahora puede generar la documentación en PDF del módulo *2*; ejecute los siguientes comandos: ::

  (sphinx)$ cd entrenamiento.desarrollo_plone/02-zope-plone-a-traves-web
  (sphinx)$ make xelatexpdf

Una ves generado el PDF se puede abrir desde el directorio ``build/latex/02-zope-plone-a-traves-web.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, ...).

Módulo 10
---------

Ahora puede generar la documentación en PDF del módulo *10*; ejecute los siguientes comandos: ::

  (sphinx)$ cd entrenamiento.desarrollo_plone/10-1-manual-multimechanize
  (sphinx)$ make xelatexpdf

Una ves generado el PDF se puede abrir desde el directorio ``build/latex/10-1-manual-multimechanize.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, ...).

Colabora
========

¿Tiene alguna idea?, ¿Encontró un error? Por favor, hágalo saber registrando un `ticket de soporte`_.

.. _entrenamiento.desarrollo_plone: https://bitbucket.org/covantec/entrenamiento.desarrollo_plone
.. _ticket de soporte: https://bitbucket.org/covantec/entrenamiento.desarrollo_plone/issues/new
