.. -*- coding: utf-8 -*-

.. _almacenamiento_multimechanize:

Almacenamiento de datos
=======================

``Multi-Mechanize``, puede almacenar los datos de prueba y sus resultados 
en base de datos, para esto requiere tener instalado previamente el
paquete `SQLAlchemy`_.

¿Qué es SQLAlchemy?
-------------------

``SQLAlchemy``, es un kit de herramientas y Mapeador Objeto Relacional para 
Python. Es una librería de abstracción de base de datos SQL para Python.

Sus puntos fuertes son:

* pleno poder y flexibilidad de SQL. ``SQLAlchemy`` provee una completa suite
  de patrones de persistencia bien conocidas de nivel empresarial, diseñadas 
  para el acceso eficiente y de alto desempeño de base de datos, adaptado 
  en un lenguaje de dominio simple y Pythonico.

* extremadamente fácil de usar para las tareas básicas, como: acceder a las 
  conexiones agrupadas, construyendo SQL desde expresiones Python, buscando 
  instancias de objetos, y haciendo modificaciones de objeto de regreso a la
  base de datos.

* lo suficientemente potente para tareas complicadas, tales como: cargar con 
  impaciencia un gráfico de objetos y sus dependencias a través de combinaciones; 
  mapear estructuras de adyacencia recursivas automáticamente; asignar objetos 
  a tablas no solo a cualquier combinación (join) arbitraria o declaración de 
  selección (select); combinar varias tablas para cargar conjuntos completos de 
  objetos que de otro modo no estarían relacionados desde un solo conjunto de 
  resultados; cometer gráficos completos de cambios de objetos en un solo paso.

* construido para ajustarse a lo que demandan los DBA, incluida la capacidad
  de intercambiar SQL generado con sentencias optimizadas a mano, uso completo 
  de parámetros de enlace para todos los valores literales, actualizaciones 
  totalmente transaccionales y consistentes utilizando la Unidad de trabajo.

* modular. Diferentes partes de ``SQLAlchemy`` pueden ser usada independientemente 
  del resto, incluyendo el pool de conexión, construcción SQL, y el ORM. ``SQLAlchemy``
  es construido en un estilo abierto que permite mucha personalización, con 
  una arquitectura que soporta tipos de datos personalizados, extensiones SQL 
  personalizadas, y plugins ORM los cuales puedan que puede aumentar o extender 
  la funcionalidad de mapping.

Para realizar la instalación ejecute el siguiente comando:

.. code-block:: sh

    $ sudo apt-get install python-sqlalchemy python-sqlalchemy-doc

.. tip::

    Alternativamente, los resultados pueden ser almacenados en una base de datos.
    Para ello debe agregar la opción ``results_database`` en el archivo de 
    configuración :file:`config.cfg`, el cual define la cadena de conexión a 
    la base de datos.

``SQLAlchemy``, soporta varios gestores de base de datos, a continuación se 
describe algunas de las configuraciones posibles:

SQLite
------

Si desea utilizar el gesto de base de datos ``SQLite``, debe instalar previamente
el soporte a Python, ejecutando el siguiente comando:

Interfaz Python para ``SQLite 3``

.. code-block:: sh

    $ sudo apt-get install python-pysqlite2

Interfaz Python para ``SQLite 2``

.. code-block:: sh

    $ sudo apt-get install python-sqlite

Un ejemplo de la configuración de conexión a bases de datos es el siguiente:

.. code-block:: cfg

    sqlite:///dbname

.. note::

    Tenga en cuenta que el soporte de ``SQLite`` es nativo en Python. Por lo tanto, 
    no es necesario su instalación y configuración.


MySQL
-----

Si desea utilizar el gesto de base de datos ``MySQL``, debe instalar previamente
el soporte a Python, ejecutando el siguiente comando:

.. code-block:: sh

    $ sudo apt-get install python-mysqldb

Un ejemplo de la configuración de conexión a bases de datos es el siguiente:

.. code-block:: cfg

    mysql://user:password@localhost/dbname


PostgreSQL
----------

Si desea utilizar el gesto de base de datos ``PostgreSQL``, debe instalar previamente
el soporte a Python, ejecutando el siguiente comando:

.. code-block:: sh

    $ sudo apt-get install python-psycopg2

Un ejemplo de la configuración de conexión a bases de datos es el siguiente:

.. code-block:: cfg

    postgresql://user:password@host:port/dbname


Microsoft SQL Server
--------------------

Si desea utilizar el gesto de base de datos ``Microsoft SQL Server``, debe instalar 
previamente el soporte a Python, ejecutando el siguiente comando:

.. code-block:: sh

    $ sudo apt-get install python-pymssql

Un ejemplo de la configuración de conexión a bases de datos es el siguiente:

.. code-block:: cfg

    mssql://mydsn

Para mayor información sobre el soporte a base de datos consulte la siguiente
dirección URL: https://multi-mechanize.readthedocs.io/en/latest/datastore.html

.. _SQLAlchemy: https://www.sqlalchemy.org/
