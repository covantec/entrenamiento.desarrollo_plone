.. -*- coding: utf-8 -*-

=====================================================
Framework para pruebas de rendimiento Multi-Mechanize
=====================================================

.. figure:: _static/multimech-450.png
   :alt: Multi-Mechanize, un framework de pruebas de rendimiento.

Multi-Mechanize, un framework de pruebas de carga, volumen y estrés.

------

**Copyright**

*Autores originales*

La documentación oficial de la librería ``Multi-Mechanize`` por Corey Goldberg.

* Copyright © 2010 - 2020 Corey Goldberg <corey@goldb.org>

Los capítulos 1, 2, 3, 4 y 5 forman parte del **Manual de uso de Multi-Mechanize** 
originalmente escrito por Milton Mazzari.

* Copyright © 2011 - 2015 Milton Mazzarri <milmazz@gmail.com>.

*Autores*

Todas esta documentación es licencia por:

* Copyright © 2015 - 2021 `Covantec R.L.`_. Algunos derechos reservados.

  * Leonardo J. Caballero G. <leonardoc@plone.org>.

.. seealso:: Ver :ref:`licencias <licencias>` para información completa sobre los términos y licencia.

.. _Fundación Plone: https://plone.org/foundation/
.. _The Plone Foundation: https://plone.org/foundation/
.. _Covantec R.L.: https://github.com/Covantec

.. toctree::
    :numbered: 1

    introduccion
    requisitos
    almacenamiento
    instalando
    comenzando
    configuracion
    ejecucion
    referencias
