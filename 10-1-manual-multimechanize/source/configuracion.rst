.. -*- coding: utf-8 -*-

.. _configuracion_multimechanize:

Configuración
=============

Cada proyecto de prueba contiene un formato de configuración en el archivo :file:`config.cfg`, 
donde establece sus opciones de prueba.

El contenido por defecto del archivo :file:`config.cfg`, lucirá similar al
siguiente: 

.. code-block:: cfg

    [global]
    run_time = 30
    rampup = 0
    results_ts_interval = 10
    progress_bar = on
    console_logging = off
    xml_report = off

    [user_group-1]
    threads = 3
    script = v_user.py

    [user_group-2]
    threads = 3
    script = v_user.py

Ejemplo de configuración
........................

A continuación, se le muestra un archivo de configuración el cual muestra todas 
las opciones posibles de configuración:

.. code-block:: cfg

    [global]
    run_time: 300
    rampup: 300
    console_logging: off
    results_ts_interval: 30
    results_database: sqlite:///projects/default_project/results.db
    post_run_script: python projects/default_project/foo.py

    [user_group-1]
    threads: 30
    script: example_mock.py

    [user_group-2]
    threads: 30
    script: example_mock.py

Opciones globales
.................

Las opciones globales del formato del archivo de configuración

* ``run_time``: Duración de la prueba en segundos.

* ``console_logging``: Activar / Desactivar el registro a la salida estándar.

* ``results_ts_interval``: Intervalos de las series de tiempo para el análisis 
  de los resultados expresadas en segundos.

* ``results_database``: Cadena de conexión a base de datos (opcional).

* ``post_run_script``: El *script* que será invocado después de la culminación 
  de la prueba (opcional).

Grupos de usuarios
..................

* ``threads``: Número de hilos / usuarios virtuales.

* ``script``: El *script* del usuario virtual a ejecutar.

Para mayor información sobre el soporte a base de datos consulte la siguiente
dirección URL: https://multi-mechanize.readthedocs.io/en/latest/configfile.html
