.. -*- coding: utf-8 -*-

.. _ejecucion_multimechanize:

Ejecución de pruebas
====================

Para facilitar la ejecución de pruebas de su proyecto, ejecute el siguiente 
comando desde la consola de comando:

.. code-block:: sh

    $ multimech-run default_project

      user_groups:  2
      threads: 6

    [================100%==================]  30s/30s   transactions: 116  timers: 116  errors: 0
    waiting for all requests to finish...


    analyzing results...

    transactions: 122
    errors: 0

    test start: 2017-11-14 09:11:57
    test finish: 2017-11-14 09:12:26

    created: ./default_project/results/results_2017.11.14_09.11.56/results.html

    done.

.. note::

    * Donde :file:`default_project` es el nombre del proyecto, el cual 
      se usara para leer las configuraciones y *scripts* de la carpeta del
      proyecto ``Multi-Mechanize``.

Este comando crea una carpeta adicional con el resultado del proyecto el cual 
lucira similar a la siguiente:

::

    ./default_project/
    ├── config.cfg
    ├── results
    │   └── results_2017.11.14_09.11.56
    │       ├── All_Transactions_response_times_intervals.png
    │       ├── All_Transactions_response_times.png
    │       ├── All_Transactions_throughput.png
    │       ├── config.cfg
    │       ├── Example_Timer_response_times_intervals.png
    │       ├── Example_Timer_response_times.png
    │       ├── Example_Timer_throughput.png
    │       ├── results.csv
    │       └── results.html
    └── test_scripts
        ├── v_user.py
        └── v_user.pyc

Para cada proyecto debe contener lo siguiente:

* :file:`config.cfg`, es el archivo de configuración del proyecto, donde establece
  sus opciones de prueba.

* :file:`results`, es el directorio para el almacenamiento de los resultados.

  .. note::

      Allí encontrará subdirectorios cuyos nombres son una estampa de tiempo y son
      creados por cada ejecución de las pruebas y contiene datos en formato CSV, un 
      sumario en formato HTML e imágenes en formato PNG.

* :file:`results/results.cvs`, es el archivo el cual almacena los resultados 
  en formato *Comma-Separated Values (CSV)*.

* :file:`results/results.html`, es el archivo el cual almacena los resultados 
  en formato *HyperText Markup Language 5 (HTML5)*.

* :file:`test_scripts`, es el directorio de almacenamiento de *scripts* de sus 
  usuarios virtuales.

* :file:`test_scripts/v_user.py`, es el modulo Python de un usuario virtual para guiar 
  la creación de sus pruebas.

Para iniciar sus pruebas el contenido de archivo de configuración :file:`config.cfg` 
puede ser similar al siguiente:

.. code-block:: cfg

    [global]
    run_time: 30
    rampup: 0
    console_logging: on
    results_ts_interval: 10

    [user_group-1]
    threads: 1
    script: v_user.py


Este archivo :file:`config.cfg` indica que se ejecutará un solo hilo (thread) 
de su usuario virtual por 30 segundos, suficiente para las pruebas iniciales
y conocer si el *script* se ejecuta tal como espera.

A partir de este momento usted debe visualizar los resultados del contador en 
la consola.

Una vez que compruebe que las cosas marchan bien, puede **desactivar** la opción
``console_logging`` e incrementar la carga en la prueba. Usted puede crear 
generar mayor carga al crear múltiples usuarios virtuales ejecutando diferentes
transacciones.

Se le recomienda ejecutar algunas iteraciones de prueba hasta que detecte cómo
responde el sistema o servicio y cuales son sus límites, por ejemplo:

.. code-block:: cfg

    [global]
    run_time: 900
    rampup: 900
    console_logging: off
    results_ts_interval: 90

    [user_group-1]
    threads: 30
    script: soap_client.py

En este caso la configuración indica que se generarán 30 hilos (threads), 
incrementando la carga a lo largo de 15 minutos.

Después de la ejecución de la prueba, un nuevo directorio es creado, dicho
directorio contiene los resultados de las pruebas. Busque el archivo 
:file:`results.html` y visualice la salida en su navegador.
