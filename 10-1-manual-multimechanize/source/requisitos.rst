.. -*- coding: utf-8 -*-

.. _requisitos_multimechanize:

Requisitos
==========


Dependencias previas
--------------------

``Multi-Mechanize`` requiere: 

- Python 2.x (2.6 o 2.7), ejecutando el siguiente comando:

    .. code-block:: sh

        $ sudo apt-get install python2.7 python2.7-dev python-pip

- Librería `Matplotlib`_, si usted desea generar gráficos a partir 
  de las pruebas, debe instalarla y sus dependencias, ejecutando el 
  siguiente comando:

    .. code-block:: sh

        $ sudo apt-get install python-matplotlib

    .. note::

        ``Matplotlib``, es una biblioteca para la generación de gráficos 
        a partir de datos contenidos en listas o arrays en el lenguaje de 
        programación Python y su extensión matemática NumPy. Proporciona 
        una API, pylab, diseñada para recordar a la de MATLAB.


- Librería `SQLAlchemy`_, si usted desea almacenar los datos de prueba 
  y sus resultados en base de datos, para esto requiere tener instalado 
  previamente el paquete ``SQLAlchemy`` y su respectivo conector al gestor 
  de base de datos que usara, ejecutando el siguiente comando:

    .. code-block:: sh

        $ sudo apt-get install python-sqlalchemy python-sqlalchemy-doc

Para mayor información vea la página de `FAQ`_.

.. note::

    Tenga en cuenta adicionalmente necesitará el módulo `mechanize`_.

.. _FAQ: https://code.google.com/archive/p/multi-mechanize/wikis/FAQ.wiki
.. _Matplotlib: https://matplotlib.org/
.. _SQLAlchemy: https://www.sqlalchemy.org/
.. _mechanize: https://pypi.org/project/mechanize/
