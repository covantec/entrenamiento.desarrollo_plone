﻿.. -*- coding: utf-8 -*-

.. _instalando_multimechanize:

Instalación
===========


Debian GNU/Linux
----------------

La instalación de los requisitos en Debian GNU/Linux y derivados
es tal como se describe a continuación:

.. code-block:: sh

    $ sudo apt-get install python-mechanize

Desde PyPI
----------

Debe instalar ``Multi-Mechanize`` desde PyPI usando Pip con el siguiente comando:

.. code-block:: sh

    $ sudo pip install -U multi-mechanize

Una vez instalando ``Multi-Mechanize``, tiene disponible tres *scripts* los cuales 
se describen a continuación:

:command:`multimech-newproject`
    Es el *script* scaffolding para crear un nuevo proyecto ``Multi-Mechanize``. 
    Para usarlo ejecute el siguiente comando: 

    .. code-block:: sh

        $ multimech-newproject <nombre-proyecto>

:command:`multimech-run`
    Es el *script* principal para ejecutar las pruebas de estrés/rendimiento. Para 
    usarlo ejecute el siguiente comando: 

    .. code-block:: sh

        $ multimech-run <nombre-proyecto> [opciones]

    Opciones

        * ``-p``, ``--port``: Puerto de escucha RPC.

    	* ``-r``, ``--results``: El directorio de resultado reprocesar.
    	
    	* ``-b``, ``--bind-addr``: La dirección de enlace RPC, valor por defecto es 
          ``localhost``.
    	
    	* ``-d``, ``--directory``: El directorio el cual contiene la carpeta de proyecto, 
          valor por defecto es: ``.``.

:command:`multimech-gridgui`
    Es el *script* de Multi-Mechanize Grid Controller, una aplicación gráfica de usuario 
    de ejemplo para controlar las instancias multi-mechanize vía la gestión api remota. 
    Para usarlo ejecute el siguiente comando: 

    .. code-block:: sh

        $ multimech-gridgui

    .. note:: 

        Usted necesita instalar el paquete ``python-tk``, para funcionar este *script*.

        .. code-block:: sh

            $ sudo apt-get install python-tk

    .. tip:: 

        La lista del juego clave y valor ``hosts:ports`` donde multi-mechanize esta escuchando
        ``192.168.1.2:9001`` y ``192.168.1.3:9001``.

De esta forma comprueba la instalación del la librería ``Multi-Mechanize``.
