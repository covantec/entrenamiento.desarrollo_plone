.. -*- coding: utf-8 -*-

.. _introduccion_multimechanize:

Introducción
============

¿Qué es Multi-Mechanize?
------------------------

`Multi-Mechanize`_, es una librería para navegar la Web en Python. `Mechanize` 
actúa como un navegador permitiéndole hacer web scraping, pruebas funcionales 
de sitios *Web* y cosas que nadie ha pensado aún. 

Es un *framework* orientado a pruebas de rendimiento y carga en sitios
*Web*. Este *framework* permite la ejecución simultánea de *scripts* en 
`Python`_ por medio de un motor multi-proceso, multi-hilo para reproducir 
secuencias de comando y generar usuarios virtuales de manera concurrente, 
todo esto con el fin de establecer carga en contra de un sitio o servicio *Web*.

Entre otras cosas, ``mechanize``:

* Seguir enlaces.

* Llenar formularios HTML.

* Observa automáticamente los archivos robots.txt.

* Tiene un historial de navegador.

Una de las grandes ventajas de ``Multi-Mechanize`` es que ofrece la posibilidad
de incluir en los *scripts* el poderoso módulo `mechanize`_ (derivado de 
`WWW::Mechanize`_ en Perl) junto con el lenguaje de programación Python. 

Permitiendo crear *scripts* de pruebas que simulan la actividad de usuarios
virtuales de manera rápida y sencilla, estos *scripts* generarán peticiones 
HTTP para navegar de forma inteligente un sitio *Web* o enviarán solicitudes
a un servicio *Web*.

Los resultados se almacenan en formato *Comma-Separated Values (CSV)*, junto
con el informe en formato *HyperText Markup Language 5 (HTML5)* que contiene
las estadísticas y gráficos.

Un ejemplo de la capacidad de reportes que ofrece Multi-Mechanize puede verla 
en `su sitio Web`_.

.. _Multi-Mechanize: https://multi-mechanize.readthedocs.io/en/latest/index.html
.. _Python: https://www.python.org/)
.. _mechanize: http://wwwsearch.sourceforge.net/mechanize/
.. _WWW::Mechanize: https://metacpan.org/pod/WWW::Mechanize
.. _su sitio Web: https://multi-mechanize.readthedocs.io/en/latest/reports.html
