.. -*- coding: utf-8 -*-

.. _referencias_multimechanize:

Referencias
===========

* Manual de uso de Multi-Mechanize, por Milton Mazzarri.

* `Performance and Scalability Testing with Python and Multi-Mechanize`_, por Corey Goldberg.

* `Python Load Testing - Pygotham 2012`_, por Dan Kuebrich.

* `Python performance profiling`_, por Jon Haddad.

* `Performing Under Pressure, pt. 1 Load-Testing With Multi-Mechanize`_, por Dan Kuebrich.

.. _Performance and Scalability Testing with Python and Multi-Mechanize: https://es.slideshare.net/coreygoldberg/performance-and-scalability-testing-with-python-and-multimechanize
.. _Python Load Testing - Pygotham 2012: https://www.slideshare.net/dkuebrich/python-load-testing-pygotham-2012
.. _Python performance profiling: https://www.slideshare.net/JonHaddad/python-performance-profiling
.. _Performing Under Pressure, pt. 1 Load-Testing With Multi-Mechanize: https://dzone.com/articles/performing-under-pressure-pt-1
