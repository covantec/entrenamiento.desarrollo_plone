.. -*- coding: utf-8 -*-

.. _comenzando_multimechanize:

Comenzando
==========

Para comenzar un proyecto de pruebas ``Multi-Mechanize``, debe crear un proyecto como 
a continuación se describe:

Crear proyecto de pruebas
-------------------------

Para crear proyecto de pruebas ``Multi-Mechanize``, ejecute el siguiente comando desde 
la línea de comando:

.. code-block:: sh

    $ multimech-newproject default_project

.. note::

    * Donde :file:`default_project` es el nombre del proyecto, el cual se usara 
      para crear la carpeta del proyecto ``Multi-Mechanize``.

.. tip::

    Luego de ejecutar el comando previo, se le recomienda analizar 
    el directorio :file:`default_project/test_scripts/` donde encontrara un modulo 
    Python de guía para iniciar la creación de sus pruebas.

Este comando crea una carpeta del proyecto será similar a la siguiente:

::

    ./default_project/
    ├── config.cfg
    └── test_scripts
        └── v_user.py

Para cada proyecto debe contener lo siguiente:

* :file:`config.cfg`, es el archivo de configuración del proyecto, donde establece
  sus opciones de prueba.

* :file:`test_scripts`, es el directorio de almacenamiento de *scripts* de sus 
  usuarios virtuales.

* :file:`test_scripts/v_user.py`, es el modulo Python de un usuario virtual para guiar 
  la creación de sus pruebas.

.. tip::

    Para múltiples proyectos, solo cree nuevo proyectos para cada uno de los mismos, 
    se le recomienda mantener la misma estructura del proyecto por omisión. 
